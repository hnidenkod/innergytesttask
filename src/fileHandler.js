import * as fileParser from './fileParser'
import { sortMaterialsByIdAscending, sortStoresByTotalCountThenByNameDescending } from './sortUtils'

const convertRawItemsToFlatRecords = (items) => {
    return items.reduce((result, currentItem) => {
        currentItem.storeDetails.forEach(s => {
            result.push({
                materialId: currentItem.materialId,
                storeName: s.storeName,
                count: s.count
            })
        })

        return result
    }, [])
}

const groupFlatRecordsToObject = (items) => {
    return items.reduce(function (result, current) {
        result[current.storeName] = result[current.storeName] || []
        result[current.storeName].push({
            materialId: current.materialId,
            count: current.count
        })

        return result
    }, {})
}

const convertAndSortResultArray = (groupedRecordsObject) => {
    const keys = Object.keys(groupedRecordsObject)
    const unsortedUtems = keys.map(key => {
        return {
            storeName: key,
            totalCountOfItems: groupedRecordsObject[key].reduce((total, currentmaterial) => {
                total += currentmaterial.count
                return total
            }, 0),
            materials: sortMaterialsByIdAscending(groupedRecordsObject[key])
        }
    })

    return sortStoresByTotalCountThenByNameDescending(unsortedUtems)
}


export const handle = (text) => {
    const parsedRawItems = fileParser.parse(text)
    const flatRecords = convertRawItemsToFlatRecords(parsedRawItems)
    const groupedObject = groupFlatRecordsToObject(flatRecords) 
    return convertAndSortResultArray(groupedObject)
}