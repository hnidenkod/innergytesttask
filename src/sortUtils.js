export const sortMaterialsByIdAscending = (materials) => {
    return materials.sort((left,right) => {
        if (left.materialId > right.materialId) {
            return 1
        } else if (right.materialId > left.materialId) {
            return -1
        } else {
            return 0
        }
    })
}

export const sortStoresByTotalCountThenByNameDescending = (unsortedUtems) => {
    return unsortedUtems.sort((left,right) => {
        if (left.totalCountOfItems < right.totalCountOfItems) {
            return 1
        } else if (right.totalCountOfItems < left.totalCountOfItems) {
            return -1
        } else {
            if (left.storeName < right.storeName) {
                return 1
            } else if (right.storeName < left.storeName) {
                return -1
            } else {
                return 0
            }
        }
    })
}