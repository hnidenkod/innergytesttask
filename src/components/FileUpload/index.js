import React from 'react'

const FileUpload = ({onFileLoaded}) => {
  const onChange = (file) => {
    const fileReader = new FileReader();
    fileReader.onloadend = () => {
      onFileLoaded(fileReader.result)
    };
    fileReader.readAsText(file);
  };
  
  return <div className='upload-wrapper'>
    <input
      type='file'
      className='input-file'
      accept='.txt'
      onChange={e => onChange(e.target.files[0])}
    />
  </div>;
};

export default FileUpload