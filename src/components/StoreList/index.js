import React from 'react'
import StoreItem from "../StoreItem";

const StoreList = ({data}) => {
    return <div className="listContainer">
        {data.map(item => <StoreItem key={`${item.storeName} (total ${item.totalCountOfItems}`} data={item} />)}
    </div>
}

export default StoreList