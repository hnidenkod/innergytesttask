import React from 'react'

const StoreItem = ({data}) => {
    return <div className='storeContainer'>
        <div className='containerRow'>
            {`${data.storeName} (total ${data.totalCountOfItems})`}
        </div>
        {data.materials.map(material => {
            return <div key={`${material.materialId}: ${material.count}`} className='containerRow'>
                {`${material.materialId}: ${material.count}`}
            </div>
         })}
    </div>
}

export default StoreItem