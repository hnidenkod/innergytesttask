import React, { useState } from 'react'
import * as fileHandler from '../fileHandler'
import FileUpload from './FileUpload'
import StoreList from './StoreList'
import "./App.scss"

const App = ({}) => {
    const [state, setState] = useState({
        isLoaded: false,
        items: []
    })

    const onFileLoaded = (fileContent) => {
        setState({
            isLoaded: true,
            items: fileHandler.handle(fileContent)
        })
    }

    const onClickClearList = () => {
        setState({
            isLoaded: false,
            items: []
        })
    }

    return (
        <>
            {state.isLoaded ? <button onClick={onClickClearList}>Clear List</button> : <FileUpload onFileLoaded={onFileLoaded} />}
            <StoreList data={state.items} />
        </>
    )
}

export default App