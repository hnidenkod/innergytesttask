const parseStoreDetails = (text) => {
    return text.split('|').map(i => {
        const [storeName, count] = i.split(',')

        return {
            storeName,
            count: +count
        }
    })
}

const parsedLines = (lines) => {
    return lines.map(line => {
        const [, materialId, storeDetails] = line.split(';')

        return {
            materialId,
            storeDetails: parseStoreDetails(storeDetails)
        }
    })
}

const getLinesWithoutCommentsAndNotEmptyOnes = (text) => {
    const newLineSeparator = '\n'
    const allLines = text.split(newLineSeparator)

    return allLines.filter(line => line && line.trim() && !line.startsWith('#'))
}

export const parse = (text) => {
    const textLines = getLinesWithoutCommentsAndNotEmptyOnes(text)
    return parsedLines(textLines)
}